#pragma newdecls required

#include <sourcemod>
#include <sdkhooks>
#include <sdktools>

#define PLUGIN_VERSION "0.8"

public Plugin myinfo =
{
	name = "Kreedz Climbing Anti-prespeed",
	author = "Besath",
	description = "Allows mappers to block prespeed on their maps",
	version = PLUGIN_VERSION,
	url = "https://bitbucket.org/besath/kzmod-antipre/"
};

enum PLUGIN_STATE
{
  PLUGIN_DISABLED,
  PLUGIN_ACTIVE
};

PLUGIN_STATE pluginState = PLUGIN_DISABLED;
float maxPre;
Handle cvarEnabled;
Handle cvarSpeed;

public void OnPluginStart()
{
  cvarEnabled = CreateConVar("ap_enabled", "0", "0 - disables anti-pre; 1 - enables anti-pre", FCVAR_NOTIFY);
  cvarSpeed = CreateConVar("ap_speed", "300.00", "Max speed allowed for prespeeding.", FCVAR_NOTIFY);
  HookEvent("player_starttimer", Event_StartTimer, EventHookMode_Pre);
  HookEvent("player_starttimer2", Event_StartTimer, EventHookMode_Pre);
  HookConVarChange(cvarEnabled, Cvar_Enabled_Changed);
  HookConVarChange(cvarSpeed, Cvar_Speed_Changed);
  // Need to set it here or it never gets set correctly for whatever reason.
  maxPre = 300.00;
}

public void Cvar_Enabled_Changed(ConVar convar, const char[] oldValue, const char[] newValue)
{
  if (strcmp(newValue, "1", false) == 0)
  {
    pluginState = PLUGIN_ACTIVE;
  }
  else
  {
    pluginState = PLUGIN_DISABLED;
  }
}

public void Cvar_Speed_Changed(ConVar convar, const char[] oldValue, const char[] newValue)
{
  maxPre = GetConVarFloat(convar);
}

public void OnMapStart()
{
  // Disable anti-pre before checking if the map/server wants to use it
  SetConVarInt(cvarEnabled, 0);
  SetConVarFloat(cvarSpeed, 300.00);

  // Check if anti-pre entity exists in the map and use it if it does
  int entIdx = FindAntiPreEntity();

  if (entIdx != -1)
  {
    SetConVarInt(cvarEnabled, 1);
    SetConVarFloat(cvarSpeed, GetEntPropFloat(entIdx, Prop_Data, "m_flSpeed"));
  }
  else
  {
    // Check in the config file next
    Handle cfg = CreateKeyValues("Prespeed");
    char path[PLATFORM_MAX_PATH];
    char currentMap[PLATFORM_MAX_PATH];
    char mapName[PLATFORM_MAX_PATH];
    GetCurrentMap(currentMap, sizeof(currentMap));
    // Remove workshop id from the map name
    SplitString(currentMap, ".ugc", mapName, sizeof(mapName));

    // If the map wasn't a workshop map, mapName will be empty
    if (mapName[0] == '\0')
      mapName = currentMap;

    BuildPath(Path_SM, path, sizeof(path), "/configs/antipre.cfg");

    if (FileExists(path))
    {
      FileToKeyValues(cfg, path);
      if (!KvGotoFirstSubKey(cfg))
      {
        PrintToServer("Couldn't read file containing anti-prespeed settings: %s", path);
      }
      else
      {
        char sectionName[64];
        do
        {
          KvGetSectionName(cfg, sectionName, sizeof(sectionName));
          if (strcmp(sectionName, mapName, false) == 0)
          {
            SetConVarInt(cvarEnabled, KvGetNum(cfg, "enabled"));
            SetConVarFloat(cvarSpeed, KvGetFloat(cfg, "speed"));
          }
        } while (KvGotoNextKey(cfg));
      }
    }
  }
}

public Action Event_StartTimer(Event event, const char[] name, bool dontBroadcast)
{
  if (pluginState == PLUGIN_DISABLED)
    return Plugin_Handled;

  int client = GetClientOfUserId(GetEventInt(event, "userid"));
  float speed = GetSpeed(client);

  if (speed > maxPre && !IsFakeClient(client))
  {
    PrintToChat(client, "Max pre allowed: %.2f. Timer stopped.", maxPre);
    FakeClientCommandEx(client, "stoptimer");
    return Plugin_Handled;
  }

  return Plugin_Continue;
}

public float GetSpeed(int client) 
{
	float vecVelocity[3];
	GetEntPropVector(client, Prop_Data, "m_vecVelocity", vecVelocity);
	vecVelocity[2] = 0.0;
	return GetVectorLength(vecVelocity);
}

public int FindAntiPreEntity()
{
  int ent = -1;
  char targetName[] = "antipre"
  char foundName[32];

  while ((ent = FindEntityByClassname(ent, "point_servercommand")) != -1)
  {
    GetEntPropString(ent, Prop_Data, "m_iName", foundName, sizeof(foundName));

    if (strcmp(targetName, foundName, false) == 0)
      break;
  }

  return ent;
}